import json


class Command:
    """
    Wrapper class for commands
    """
    def __init__(self, command, params, token):
        self.__command = command
        self.__params = params
        self.__token = token

    def __str__(self):
        command = json.dumps({
            "command": self.__command,
            "params": self.__params,
            "token": self.__token
        })
        return "%d:%s," % (len(command.encode()), command)

    def encode(self):
        return self.__str__().encode()
