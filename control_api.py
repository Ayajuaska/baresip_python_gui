import json
from PyQt5.QtCore import QObject, pyqtSignal

from command import Command
from connection_worker import ConnectionWorker
import logging
from random import randint


class Token:

    REGISTER_TOKEN = 'register%d' % randint(0, 2**32)
    UA_DELETE = 'uadel%d' % randint(0, 2**32)


class Event:
    REGISTERING = "REGISTERING"
    REGISTER_OK = "REGISTER_OK"
    REGISTER_FAIL = "REGISTER_FAIL"
    UNREGISTERING = "UNREGISTERING"
    MWI_NOTIFY = "MWI_NOTIFY"
    SHUTDOWN = "SHUTDOWN"
    EXIT = "EXIT"
    CALL_INCOMING = "CALL_INCOMING"
    CALL_RINGING = "CALL_RINGING"
    CALL_PROGRESS = "CALL_PROGRESS"
    CALL_ESTABLISHED = "CALL_ESTABLISHED"
    CALL_CLOSED = "CALL_CLOSED"
    CALL_TRANSFER = "CALL_TRANSFER"
    CALL_TRANSFER_FAILED = "CALL_TRANSFER_FAILED"
    CALL_DTMF_START = "CALL_DTMF_START"
    CALL_DTMF_END = "CALL_DTMF_END"
    CALL_RTCP = "CALL_RTCP"
    CALL_MENC = "CALL_MENC"
    VU_TX = "VU_TX"
    VU_RX = "VU_RX"
    AUDIO_ERROR = "AUDIO_ERROR"


class ControlApi(QObject):
    """
    API/Middleware between UI and Network layer
    """
    sip_register = pyqtSignal(str, str, str)
    connection_changed = pyqtSignal(bool)
    update_status = pyqtSignal(str)

    sip_registered = pyqtSignal()
    sip_registering = pyqtSignal()
    sip_unregistered = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.token_callbacks = {
            Token.REGISTER_TOKEN: self.register_callback,
            Token.UA_DELETE: self.ua_delete_callback,
        }
        self.logger = logging.getLogger(self.__class__.__name__)
        self.connection = ConnectionWorker()
        self.connect_signals()
        self.connection.do_connect()

    def connect_signals(self):
        """
        Connect signals and slots in one place
        """
        self.connection.onTryConnect.connect(self.on_connect)
        self.connection.gotResponse.connect(self.parse_response)
        self.sip_register.connect(self.do_register)

    def on_connect(self, is_ok: bool):
        """
        Slot for connection state change signal
        :param is_ok: connected or not flag
        """
        self.logger.debug(is_ok)
        if is_ok:
            message = "Connected"
        else:
            message = "Disconnected"
        self.connection_changed.emit(is_ok)
        self.update_status.emit(message)

    def do_register(self, host: str, name: str, password: str):
        """
        Prepare uaregister message and send it to connection worker
        :param host: Host to register
        :param name: Authentication name
        :param password: Authentication password
        """
        msg = Command("uanew", "sip:%s@%s;auth_pass=%s" % (name, host, password), Token.REGISTER_TOKEN)
        self.logger.debug("Register message: %s", msg)
        self.connection.sendCommand.emit(msg)
        self.sip_registering.emit()

    def do_unregister(self, host: str, name: str):
        """
        Unregister and delete UA
        :param host: Target host
        :param name: Username
        """
        self.logger.debug('Unregistering %s@%s', name, host)
        self.sip_registering.emit()
        self.user_agent_delete('sip:%s@%s' % (name, host))

    def parse_response(self, response: bytes):
        """
        Parse answer from network and call function
        :param response: response from socket
        """
        counter = 0
        self.logger.debug("\nGot response: %s", response)
        while response:
            if counter > 0:
                self.logger.debug('Got %d messages', counter + 1)
                pass
            counter += 1
            lenght: int = 0
            for c in response.decode():
                if not c.isdigit():
                    break
                lenght = lenght * 10 + int(c)

            if lenght < 1:
                self.logger.debug("Got message with lenght = %d" % lenght)
                self.sip_unregistered.emit()
                continue
            msg_start = response.index(ord(':')) + 1
            try:
                doc = json.loads(response[msg_start:msg_start + lenght])
            except json.JSONDecodeError as err:
                self.logger.error("Response decode error: %s", err)
                return
            response = response[msg_start + lenght + 1:]

            if response:
                self.logger.debug('Msg tail: %s' % response)
            self.logger.debug("doc = %s", doc)

            if 'event' in doc:
                # Dispatch event
                self.event_callback(doc)
                continue

            if 'token' in doc:
                if doc['token'] in self.token_callbacks:
                    self.token_callbacks[doc['token']](doc)
                continue

    def register_callback(self, msg: dict):
        """
        Callback for register message response
        :param msg: message
        """
        if not msg['ok']:
            status = "Register failed"
            self.sip_unregistered.emit()
        else:
            status = "Registered"
            self.sip_registered.emit()

        self.update_status.emit(status)
        if 'data' in msg:
            self.logger.debug("%s: %s", status, msg['data'])

    def ua_delete_callback(self, msg: dict):
        """
        Callback for user agent delete response
        :param msg: message
        """
        self.logger.debug('UA deleted: %s', msg.get('data', ''))
        self.sip_unregistered.emit()

    def event_callback(self, msg):
        """
        Events dispatching
        :param msg: event message
        """
        self.logger.debug("Event: %s", msg)
        msg_type = msg.get('type')
        if not msg_type:
            self.logger.debug("No 'type' in %s", msg)
        if msg_type == Event.REGISTER_FAIL:
            if msg.get('accountaor') is not None:
                self.user_agent_delete(msg['accountaor'])
            self.sip_unregistered.emit()
        elif msg_type in (Event.REGISTERING, Event.UNREGISTERING):
            self.sip_registering.emit()
        elif msg_type == Event.REGISTER_OK:
            self.sip_registered.emit()

    def user_agent_delete(self, user_agent):
        """
        Delete user agent and unregister
        :param user_agent: useragent string, ex. 'sip:user@host'
        """
        cmd = Command('uadel', user_agent, Token.UA_DELETE)
        self.logger.debug(cmd)
        self.connection.sendCommand.emit(cmd)

    # ################################################## #
    def debug(self):
        """
        For debugging purposes
        """
        cmd = json.dumps({
            "command": "reginfo",
            "params": "",
            "token": "hzhzhz"
        })

        self.connection.sendCommand.emit("%d:%s," % (len(cmd.encode()), cmd))
