from PyQt5.QtCore import QStateMachine, QState, pyqtSignal


class RegisterSM(QStateMachine):
    """
    State machine for registration process
    """
    sig_registering = pyqtSignal()

    def __init__(self, parent=None):
        """
        :param parent: MainWindow
        :type parent: main_window.MainWindow
        """
        super().__init__(parent)
        self.unregistered = QState(self)
        self.registering = QState(self)
        self.registered = QState(self)

        self.unregistered.addTransition(parent.registering, self.registering)
        self.registering.addTransition(parent.unregistered, self.unregistered)
        self.registering.addTransition(parent.registered, self.registered)
        self.registered.addTransition(parent.unregistered, self.unregistered)

        self.unregistered.assignProperty(parent.ui.registerBtn, "enabled", True)
        self.unregistered.assignProperty(parent.ui.unregisterBtn, "enabled", False)
        self.unregistered.assignProperty(parent.ui.hostIn, "enabled", True)
        self.unregistered.assignProperty(parent.ui.userIn, "enabled", True)
        self.unregistered.assignProperty(parent.ui.passIn, "enabled", True)
        self.unregistered.assignProperty(parent.ui.regStatus, "checked", False)

        self.registering.assignProperty(parent.ui.registerBtn, "enabled", False)
        self.registering.assignProperty(parent.ui.unregisterBtn, "enabled", False)
        self.registering.assignProperty(parent.ui.hostIn, "enabled", False)
        self.registering.assignProperty(parent.ui.userIn, "enabled", False)
        self.registering.assignProperty(parent.ui.passIn, "enabled", False)
        self.registering.assignProperty(parent.ui.regStatus, "checked", False)

        self.registered.assignProperty(parent.ui.registerBtn, "enabled", False)
        self.registered.assignProperty(parent.ui.unregisterBtn, "enabled", True)
        self.registered.assignProperty(parent.ui.hostIn, "enabled", False)
        self.registered.assignProperty(parent.ui.userIn, "enabled", False)
        self.registered.assignProperty(parent.ui.passIn, "enabled", False)
        self.registered.assignProperty(parent.ui.regStatus, "checked", True)

        self.setInitialState(self.unregistered)

        self.start()
