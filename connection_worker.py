import logging

from PyQt5.QtCore import QThread, pyqtSignal
from PyQt5.QtNetwork import QTcpSocket

from command import Command

SLEEP_TIMEOUT = 19


class ConnectionWorker(QThread):
    """
    Class for low-level communicating with baresip via tcp
    """
    TCP_HOST = '127.0.0.1'
    TCP_PORT = 4444

    tryConnect = pyqtSignal()
    onTryConnect = pyqtSignal(bool, name="isOk")
    sendCommand = pyqtSignal(Command, name="command")
    gotResponse = pyqtSignal(bytes, name="response")

    def __init__(self, parent=None):
        QThread.__init__(self, parent)
        self.logger = logging.getLogger(self.__class__.__name__)
        self.__socket = QTcpSocket(self)
        self.tryConnect.connect(self.do_connect)

        self.__socket.connected.connect(lambda : self.onTryConnect.emit(True))
        self.__socket.disconnected.connect(lambda : self.onTryConnect.emit(False))
        self.__socket.readyRead.connect(self.on_ready_read)
        self.sendCommand.connect(self.do_send_command)

        self.start()

    def do_connect(self):
        """
        Just connect
        """
        if self.__socket.state() not in  (QTcpSocket.ConnectedState, QTcpSocket.ConnectingState,):
            self.__socket.connectToHost(self.TCP_HOST, self.TCP_PORT)

    def do_send_command(self, command: Command):
        """
        Write command to socket
        """
        self.__socket.write(command.encode())

    def on_ready_read(self):
        """
        Read bytes from socket and send it to consumer as string
        """
        response_raw = self.__socket.readAll()
        if response_raw.length() > 0:
            self.logger.debug("Read: %s", response_raw)
            response = bytes(response_raw)
            self.gotResponse.emit(response)

    def run(self):
        """
        Thread's main loop
        Just checking if we're still connected, otherwise - reconnect
        """
        while True:
            if self.__socket.state() not in (QTcpSocket.ConnectedState, QTcpSocket.ConnectingState):
                self.tryConnect.emit()
                self.sleep(SLEEP_TIMEOUT)
