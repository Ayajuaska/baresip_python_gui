import logging
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QMainWindow
from PyQt5 import uic
from control_api import ControlApi
from state_machines.register_sm import RegisterSM


class MainWindow(QMainWindow):

    unregistered = pyqtSignal()
    registering = pyqtSignal()
    registered = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.logger = logging.getLogger(self.__class__.__name__)
        self.ui = uic.loadUi('mainwindow.ui', self)
        self.ui.show()
        self.api = ControlApi()
        self.register_sm = RegisterSM(self)

        self.api.update_status.connect(self.update_status)
        self.api.connection_changed.connect(self.on_connect)
        self.api.sip_unregistered.connect(self.unregistered)
        self.api.sip_registering.connect(self.registering)
        self.api.sip_registered.connect(self.registered)

        self.ui.registerBtn.clicked.connect(self.on_register_button_click)
        self.ui.unregisterBtn.clicked.connect(self.on_unregister_button_click)

        self.unregistered.connect(lambda: self.logger.debug('Entered unregistered state'))
        self.registering.connect(lambda: self.logger.debug('Entered registering state'))
        self.registered.connect(lambda: self.logger.debug('Entered registered state'))

    def on_connect(self, is_ok: bool):
        self.ui.centralWidget.setEnabled(is_ok)

    def update_status(self, status: str):
        """
        Update text in status bar
        """
        self.ui.statusBar.showMessage(status)

    def on_register_button_click(self, __):
        """
        Callback for click event on register button
        """
        self.api.do_register(self.ui.hostIn.text(), self.ui.userIn.text(), self.ui.passIn.text())

    def on_unregister_button_click(self, __):
        """
        Callback for click event on unregister button
        """
        self.api.do_unregister(self.ui.hostIn.text(), self.ui.userIn.text())
