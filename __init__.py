from PyQt5.QtWidgets import QApplication
from main_window import MainWindow
import logging


def main():
    import sys
    app = QApplication(sys.argv)
    mw = MainWindow()
    sys.exit(app.exec())


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    main()
